# Bokun sign-up

## Getting started

npx playwright install

## Test run

npx playwright test --ui

## Description

The test covers one use case when user registers as operator.

Additional happy path tests may be created:

- to register as reseller
- to register as both (operator and reseller)

The Viator option enabled / disabled, website available / not available, onboarding templates testing conditions should be taken into consideration as well.

## Notes

Page objects model was not introduced into the demo project.
