import { test, expect } from "@playwright/test";

test("User should be able to register as operator successfully", async ({
  page
}) => {
  await page.goto("https://extranet.bokuntest.com/register");
  await page.waitForLoadState();

  //fill step 1 of 2 input fields
  //await page.locator("#firstName").fill("a");
  await page.getByPlaceholder("John", { exact: true }).fill("a");
  await page.getByPlaceholder("Doe", { exact: true }).fill("aa");
  const ending = Date.now();
  await page.getByPlaceholder("John.Doe@gmail.com").fill(`aa+${ending}@aa.aa`);
  await page.getByLabel("Password").fill("aaaaaa");
  await page.getByTestId("get-started-btn").click();

  //fill step 2 of 2 input fields
  await expect(page.getByTestId("go-back-btn")).toBeVisible();
  await page.getByPlaceholder("John Doe Tours").fill("a");
  await page.getByPlaceholder("www.johndoetours.com").fill("www.aa");
  await page
    .getByRole("option", {
      name: "Vilnius, Vilnius City Municipality, Lithuania"
    })
    .click();
  await page.getByLabel("Phone").click();
  await page.getByLabel("Phone").fill("650 80000");
  await page.getByTestId("free-trial-btn").click();
  await page.waitForLoadState();

  //verify user may navigate to dashboard page
  await expect(page.getByRole("button", { name: "Let's go" })).toBeVisible({
    timeout: 25000
  });
  await page.getByRole("button", { name: "Complete later" }).click();
  await page.waitForLoadState();
  await expect(page.locator("#sideNavAside")).toContainText("Bookings", {
    timeout: 25000
  });
});
